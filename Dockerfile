# 設定基礎映象 
FROM nginx
# 定義作者
MAINTAINER rain <rain@redpay.io>
# 將dist檔案中的內容複製到 /usr/share/nginx/html/ 這個目錄下面
COPY dist/  /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf